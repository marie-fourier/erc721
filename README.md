# NFT
- Написать контракты NFT стандартов ERC-721, ERC-1155 совместимые с opensea. Можно наследовать паттерн у openzeppelin. 
- Написать контракт NFT
- Написать полноценные тесты к контракту
- Написать скрипт деплоя
- Задеплоить в тестовую сеть
- Написать таск на mint
- Верифицировать контракт
- Загрузить какой либо файл на ipfs
- Вставить в контракт NFT ссылку на ipfs

Требования
- Все предусмотренные стандартами ERC-721, ERC-1155 функции
- Все данные NFT должны отображаться на opensea

Metadata Standards
How to add rich metadata to your ERC721 or ERC1155 NFTs

https://docs.opensea.io/docs/metadata-standards
# Example
721 NFT Collection: https://testnets.opensea.io/collection/blueprints-v3-1

721 Verified NFT Contract: https://rinkeby.etherscan.io/address/0x91d9c5987ca8b6893f95701243af9cef976abe9a#code

1155 NFT Collection: https://testnets.opensea.io/collection/blueprints-1155-v3

1155 Verifiy NFT Contract: https://rinkeby.etherscan.io/address/0xee4b4231e09c01ac4b7996bb02b463972563c536

# References
https://eips.ethereum.org/EIPS/eip-721

https://docs.opensea.io/docs/creating-an-nft-contract