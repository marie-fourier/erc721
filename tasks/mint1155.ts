import { task } from "hardhat/config";
import { getContract1155 } from "../helpers";

interface TaskArgs {
  to: string;
  id: Number;
}

task("mint1155")
  .addParam("to", "Address")
  .addParam("id", "Token id")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract1155(hre);
    try {
      await contract.mint(taskArgs.to, taskArgs.id, 1);
      console.log("Minted NFT");
    } catch (err) {
      console.log("Error minting", err);
    }
  });
