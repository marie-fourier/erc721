import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  to: string;
  id: Number;
}

task("mint")
  .addParam("to", "Address")
  .addParam("id", "Token id")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    try {
      await contract.mint(taskArgs.to, taskArgs.id);
      console.log("Minted NFT");
    } catch (err) {
      console.log("Error minting");
    }
  });
