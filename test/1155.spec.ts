import { expect } from "chai";
import { ethers } from "hardhat";

describe("ERC1155", async () => {
  let owner: any, account1: any, contract: any;

  before(async () => {
    const Contract = await ethers.getContractFactory("BlueprintsNFT1155");
    [owner, account1] = await ethers.getSigners();
    contract = await Contract.deploy();
    await contract.deployed();
  });

  it("only owner should mint", async () => {
    await expect(contract.connect(account1).mint(account1.address, 1, 1)).to.be
      .reverted;
    await contract.mint(owner.address, 1, 1);
  });

  it("should have correct tokenUri", async () => {
    expect(await contract.uri(1)).to.eq(
      "ipfs://bafybeidjvkbhgejass74mcrtvry5wfrnzy23iypngiwztywwwrwd27aat4/1"
    );
  });
});
