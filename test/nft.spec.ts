import { expect } from "chai";
import { ethers } from "hardhat";

describe("ERC721", async () => {
  let owner: any, account1: any, contract: any;

  before(async () => {
    const Contract = await ethers.getContractFactory("BlueprintsNFT");
    [owner, account1] = await ethers.getSigners();
    contract = await Contract.deploy();
    await contract.deployed();
  });

  it("only owner should mint", async () => {
    await expect(contract.connect(account1).mint(account1.address, 1)).to.be
      .reverted;
    await contract.mint(owner.address, 1);
  });

  it("should have correct tokenUri", async () => {
    expect(await contract.tokenURI(1)).to.eq(
      "https://nftstorage.link/ipfs/bafybeidjvkbhgejass74mcrtvry5wfrnzy23iypngiwztywwwrwd27aat4/1"
    );
  });
});
