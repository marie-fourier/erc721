// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract BlueprintsNFT1155 is ERC1155, Ownable {
  string public name = "Blueprints 1155";
  string private _baseUri = "ipfs://bafybeidjvkbhgejass74mcrtvry5wfrnzy23iypngiwztywwwrwd27aat4/";

  constructor() ERC1155(_baseUri) {}

  function mint(address to, uint256 tokenId, uint256 amount) external onlyOwner {
    _mint(to, tokenId, amount, "");
  }

  function uri(uint256 tokenId) override public view returns (string memory) {
    return string(
      abi.encodePacked(
        _baseUri,
        Strings.toString(tokenId)
      )
    );
  }
}