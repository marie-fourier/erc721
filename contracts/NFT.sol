// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract BlueprintsNFT is ERC721, Ownable {
  constructor() ERC721("Blueprints", "BLP") {}

  function _baseURI() internal pure override returns (string memory) {
    return "https://nftstorage.link/ipfs/bafybeidjvkbhgejass74mcrtvry5wfrnzy23iypngiwztywwwrwd27aat4/";
  }

  function mint(address to, uint256 tokenId) external onlyOwner {
    _mint(to, tokenId);
  }
}